package land.chipmunk.chipmunkbot.util;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

public class CPUInfo {
  public static Map<String, String> readCpuInfo () throws IOException {
    return readCpuInfo(new File("/proc/cpuinfo"));
  }

  public static Map<String, String> readCpuInfo (File file) throws IOException {
    final InputStream opt = new FileInputStream(file);
    final Reader reader = new BufferedReader(new InputStreamReader(opt));

    return readCpuInfo(reader);
  }

  public static Map<String, String> readCpuInfo (Reader reader) throws IOException {
    final Map<String, String> map = new HashMap<>();

    do {
      final String key = ((char) reader.read()) + readUntil(reader, ':').trim();
      final String value = readUntil(reader, '\n').trim();

      if (!key.isEmpty()) map.put(key, value);
    } while (reader.ready());

    return map;
  }

  private static String readUntil (Reader reader, char character) throws IOException {
    StringBuilder stringBuilder = new StringBuilder();

    while (true) {
      int readInt = reader.read();
      if (readInt == -1 || (char) readInt == character) break;
      stringBuilder.append((char) readInt);
    }

    return stringBuilder.toString();
  }
}
