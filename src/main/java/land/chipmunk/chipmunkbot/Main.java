package land.chipmunk.chipmunkbot;

import com.github.steveice10.mc.protocol.packet.ingame.clientbound.ClientboundLoginPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.ServerboundChatPacket;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.packet.Packet;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.List;
import java.util.ArrayList;

import com.google.gson.Gson;

public class Main {
  private static Configuration getConfig (File file) throws IOException {
    final Gson gson = new Gson();

    if (!file.exists()) {
      // Read the default config
      InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("default_config.json");
      BufferedReader reader = new BufferedReader(new InputStreamReader(is));
      StringBuilder stringBuilder = new StringBuilder();
      while (reader.ready()) {
        char character = (char) reader.read();
        stringBuilder.append(character);
      }
      String defaultConfig = stringBuilder.toString();

      // Write the default config
      BufferedWriter configWriter = new BufferedWriter(new FileWriter(file));
      configWriter.write(defaultConfig);
      configWriter.close();

      System.out.println("The config.json file was not found, so a default one was created.");

      // Return the default config (instead of reading again, for efficiency)
      return gson.fromJson(defaultConfig, Configuration.class);
    }

    InputStream opt = new FileInputStream(file);
    BufferedReader reader = new BufferedReader(new InputStreamReader(opt));

    return gson.fromJson(reader, Configuration.class);
  }

  public static void main (String[] arguments) {
    System.out.println("ChipmunkBot is starting...");

    Configuration config = null;
    try {
      config = getConfig(new File(arguments.length > 0 ? arguments[0] : "config.json"));
    } catch (Exception exception) {
      exception.printStackTrace();
      System.exit(1);
    }

    List<Client> allClients = new ArrayList<>();

    for (Options options : config.bots) {
      final Client client = new ChipmunkBot(options, allClients);
      allClients.add(client);
    }
  }

  private static class Configuration {
    public Options[] bots;
  }
}
