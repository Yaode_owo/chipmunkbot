package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.data.chat.PlayerMessage;
import land.chipmunk.chipmunkbot.data.chat.SystemChatParser;
import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import land.chipmunk.chipmunkbot.util.UUIDUtilities;
import com.github.steveice10.mc.protocol.packet.ingame.clientbound.ClientboundSystemChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.clientbound.ClientboundPlayerChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.clientbound.ClientboundDisguisedChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.ServerboundChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.ServerboundChatCommandPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.inventory.ServerboundSetCreativeModeSlotPacket;
import com.github.steveice10.packetlib.packet.Packet;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.event.session.SessionListener;
import com.github.steveice10.mc.protocol.data.game.entity.metadata.ItemStack;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;
import com.github.steveice10.mc.protocol.MinecraftProtocol;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import lombok.Getter;
import java.util.BitSet;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.time.Instant;
import java.nio.charset.StandardCharsets;

import land.chipmunk.chipmunkbot.systemChat.*;

public class ChatPlugin extends SessionAdapter {
  private final ChipmunkBot client;
  @Getter private List<Listener> listeners = new ArrayList<>();

  private List<SystemChatParser> systemChatParsers;

  public ChatPlugin (ChipmunkBot client) {
    this.client = client;
    client.addListener((SessionListener) this);

    systemChatParsers = new ArrayList<>();
    systemChatParsers.add(new MinecraftChatParser(client));
    systemChatParsers.add(new KaboomChatParser(client));
  }

  public void message (String message) {
    final ServerboundChatPacket packet = new ServerboundChatPacket(message, Instant.now().toEpochMilli(), 0, null, 0, new BitSet());
    client.session().send(packet);
  }

  public void command (String command) {
    final ServerboundChatCommandPacket packet = new ServerboundChatCommandPacket(command, Instant.now().toEpochMilli(), 0, Collections.emptyList(), 0, new BitSet());
    client.session().send(packet);
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundSystemChatPacket) packetReceived(session, (ClientboundSystemChatPacket) packet);
    else if (packet instanceof ClientboundPlayerChatPacket) packetReceived(session, (ClientboundPlayerChatPacket) packet);
    else if (packet instanceof ClientboundDisguisedChatPacket) packetReceived(session, (ClientboundDisguisedChatPacket) packet);
  }

  public void packetReceived (Session session, ClientboundSystemChatPacket packet) {
    final Component component = packet.getContent();
    final boolean overlay = packet.isOverlay();

    PlayerMessage playerMessage = null;

    for (SystemChatParser parser : systemChatParsers) {
      playerMessage = parser.parse(component);
      if (playerMessage != null) break;
    }

    for (Listener listener : this.listeners) {
      listener.systemMessageReceived(component, overlay);
      if (playerMessage != null) listener.playerMessageReceived(playerMessage);
    }
  }

  public void packetReceived (Session session, ClientboundPlayerChatPacket packet) {
    final MutablePlayerListEntry sender = client.playerList().getEntry(packet.getSender());
    if (sender == null) return;

    PlayerMessage parsedFromMessage = null;
    final Component component = packet.getUnsignedContent();

    for (SystemChatParser parser : systemChatParsers) {
      parsedFromMessage = parser.parse(component);
      if (parsedFromMessage != null) break;
    }

    if (parsedFromMessage == null) return;

    final PlayerMessage playerMessage = new PlayerMessage(sender, parsedFromMessage.contents(), "minecraft:chat", packet.getName()); // TODO: Fix chatType

    for (Listener listener : this.listeners) {
      listener.playerMessageReceived(playerMessage);
    }
  }

  public void packetReceived (Session session, ClientboundDisguisedChatPacket packet) {
    PlayerMessage parsedFromMessage = null;
    final Component component = packet.getMessage();

    for (SystemChatParser parser : systemChatParsers) {
      parsedFromMessage = parser.parse(component);
      if (parsedFromMessage != null) break;
    }

    if (parsedFromMessage == null) return;

    final PlayerMessage playerMessage = new PlayerMessage(parsedFromMessage.sender(), parsedFromMessage.contents(), "minecraft:chat", packet.getName()); // TODO: Fix chatType

    for (Listener listener : this.listeners) {
      listener.playerMessageReceived(playerMessage);
    }
  }

  // ? Should this be here?
  // TODO: Break up the method to make it less messy
  public void tellraw (Component message, String targets) {
    final int maxLength = client.core().maxCommandLength();

    final String raw = GsonComponentSerializer.gson().serialize(message);
    String command = "minecraft:tellraw " + targets + " " + raw;

    if (command.length() > maxLength) {
      String tagString;
      boolean interpret;

      if (message instanceof TextComponent && message.style().isEmpty() && (message.children() == null || message.children().size() == 0)) {
        tagString = ((TextComponent) message).content();
        interpret = false;
      } else {
        tagString = raw;
        interpret = true;
      }

      if (tagString.getBytes(StandardCharsets.UTF_8).length > 65535) return;

      CompoundTag itemTag = new CompoundTag("");
      itemTag.put(new StringTag("m", tagString));

      final Session session = client.session();

      session.send(new ServerboundSetCreativeModeSlotPacket(26, new ItemStack(1 /* stone */, 1, itemTag)));
      client.core().run("minecraft:tellraw " + targets + " {\"nbt\":\"Inventory[0].tag.m\",\"entity\":\"" + client.profile().getIdAsString() + "\",\"interpret\":" + interpret + "}"); // TODO: Use GSON instead of concatenating strings, and hardcode less of this (it shouldn't matter here but yes)

      return;
    }

    client.core().run(command);
  }
  public void tellraw (Component message) { tellraw(message, "@a"); }
  public void tellraw (Component message, UUID uuid) { tellraw(message, UUIDUtilities.selector(uuid)); }

  public static class Listener {
    public void systemMessageReceived (Component component, boolean overlay) {}
    public void playerMessageReceived (PlayerMessage message) {}
  }

  public void addListener (Listener listener) { listeners.add(listener); }
  public void removeListener (Listener listener) { listeners.remove(listener); }
}
