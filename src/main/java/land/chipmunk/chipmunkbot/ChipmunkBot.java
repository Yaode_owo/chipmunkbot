package land.chipmunk.chipmunkbot;

import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.packetlib.ProxyInfo;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.tcp.TcpClientSession;
import com.github.steveice10.packetlib.event.session.SessionListener;
import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.Constructor;
import lombok.Getter;
import land.chipmunk.chipmunkbot.plugins.*;
import java.util.List;

public class ChipmunkBot extends Client {
  @Getter private final TickLoop tickLoop;
  @Getter private final ChatPlugin chat;
  @Getter private final TabCompletePlugin tabComplete;
  @Getter private final QueryPlugin query;
  @Getter private final PlayerListPlugin playerList;
  @Getter private final CommandSpyPlugin commandSpy;
  @Getter private final CommandManager commandManager;
  @Getter private final PlayerCommandHandler playerCommandHandler;
  @Getter private final PositionManager position;
  @Getter private final CommandCore core;
  @Getter private final SelfCarePlugin selfCare;
  @Getter private final SongPlayer songPlayer;

  public ChipmunkBot (Options options, List<Client> allClients) {
    super(options.host, options.port, new MinecraftProtocol(options.username), null, options.reconnectDelay, allClients);

    this.tickLoop = new TickLoop(this);
    this.chat = new ChatPlugin(this);
    this.tabComplete = new TabCompletePlugin(this);
    this.query = new QueryPlugin(this);
    this.playerList = new PlayerListPlugin(this);
    this.commandSpy = new CommandSpyPlugin(this);
    this.commandManager = new CommandManager(this);
    this.playerCommandHandler = new PlayerCommandHandler(this, options);
    this.position = new PositionManager(this);
    this.core = new CommandCore(this, options);
    this.selfCare = new SelfCarePlugin(this);
    this.songPlayer = new SongPlayer(this);
  }
}
