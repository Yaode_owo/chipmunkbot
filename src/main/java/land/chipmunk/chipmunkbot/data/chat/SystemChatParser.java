package land.chipmunk.chipmunkbot.data.chat;

import net.kyori.adventure.text.Component;

public interface SystemChatParser {
  PlayerMessage parse (Component message);
}