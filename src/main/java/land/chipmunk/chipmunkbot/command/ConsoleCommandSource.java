package land.chipmunk.chipmunkbot.command;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;

public class ConsoleCommandSource extends CommandSource {
  public ConsoleCommandSource (ChipmunkBot client) { super(client); }

  @Override
  public void sendOutput (Component message, boolean broadcast) {
    System.out.println(message);

    if (broadcast) client().chat().tellraw(Component.translatable("chat.type.admin", displayName(), message).color(NamedTextColor.GRAY).decoration(TextDecoration.ITALIC, true));
  }

  @Override
  public Component displayName () {
    return Component.text("Console");
  }
}
