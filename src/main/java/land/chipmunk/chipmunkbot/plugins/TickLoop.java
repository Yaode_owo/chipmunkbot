package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.Client;
import com.github.steveice10.packetlib.event.session.SessionListener;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.event.session.DisconnectedEvent;
import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.mc.protocol.data.ProtocolState;
import lombok.Getter;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TickLoop extends SessionAdapter {
  private Client client;
  private Timer timer;
  @Getter private List<Listener> listeners = new ArrayList<>();
  @Getter private long tick = 0;

  public TickLoop (Client client) {
    this.client = client;
    client.session().addListener((SessionListener) this);

    final TimerTask task = new TimerTask() {
      public void run () {
        for (Listener listener : listeners) {
          listener.onTick(tick);
          if (((MinecraftProtocol) client.session().getPacketProtocol()).getState() == ProtocolState.GAME) listener.onGameTick(tick);
        }
      }
    };

    timer = new Timer();
    timer.schedule(task, 50L, 50L);
  }

  public void disconnected (DisconnectedEvent event) {
    if (client.reconnectDelay() < 0 && timer != null) {
      timer.cancel();
      timer.purge();
    }
  }

  public static class Listener {
    public void onTick (long tick) {}
    public void onGameTick (long tick) {}
  }

  public void addListener (Listener listener) { listeners.add(listener); }
  public void removeListener (Listener listener) { listeners.remove(listener); }
}
