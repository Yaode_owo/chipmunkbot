package land.chipmunk.chipmunkbot;

public class Options {
  public String host = "0.0.0.0";
  public int port = 25565;
  public String username = "Player";
  // public ProxyInfo proxy;
  public long reconnectDelay = 1000;

  public Commands commands = new Commands();

  public Core core = new Core();

  public class Commands {
    public String prefix = "default.";
    public String cspyPrefix = "default.";
  }

  public class Core {
    public boolean enabled = true;
  }
}
