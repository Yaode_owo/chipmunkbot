package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.Client;
import org.cloudburstmc.math.vector.Vector3d;
import com.github.steveice10.packetlib.packet.Packet;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.SessionListener;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.mc.protocol.packet.ingame.clientbound.entity.player.ClientboundPlayerPositionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.level.ServerboundAcceptTeleportationPacket;
import org.cloudburstmc.math.vector.Vector3d;
import org.cloudburstmc.math.vector.Vector3i;
import lombok.Getter;
import lombok.Setter;

public class PositionManager extends SessionAdapter {
  private final Client client;

  @Getter @Setter double x;
  @Getter @Setter double y;
  @Getter @Setter double z;
  @Getter @Setter float yaw;
  @Getter @Setter float pitch;

  public PositionManager (Client client) {
    this.client = client;
    client.addListener((SessionListener) this);
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundPlayerPositionPacket) packetReceived((ClientboundPlayerPositionPacket) packet, session);
  }

  public void packetReceived (ClientboundPlayerPositionPacket packet, Session session) {
    // TODO: Relative positions
    x = packet.getX();
    y = packet.getY();
    z = packet.getZ();
    yaw = packet.getYaw();
    pitch = packet.getPitch();

    client.session().send(new ServerboundAcceptTeleportationPacket(packet.getTeleportId()));
  }

  public Vector3d vector3d () { return Vector3d.from(x, y, z); }
  public Vector3i vector3i () { return Vector3i.from((int) x, (int) y, (int) z); }
}
