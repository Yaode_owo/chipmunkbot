package land.chipmunk.chipmunkbot.command;

import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import net.kyori.adventure.text.Component;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import lombok.Getter;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CommandSource {
  private static final SimpleCommandExceptionType PLAYER_NOT_FOUND = new SimpleCommandExceptionType(ComponentMessage.wrap(Component.translatable("argument.entity.notfound.player")));

  @Getter private final ChipmunkBot client;

  // ? Should I support message objects?
  public void sendOutput (Component message, boolean broadcast) {}
  public void sendOutput (Component message) { sendOutput(message, true); }

  public Component displayName () { return Component.empty(); }

  public MutablePlayerListEntry player () { return null; }

  public MutablePlayerListEntry playerOrThrow () throws CommandSyntaxException {
    MutablePlayerListEntry player = player();
    if (player == null) throw PLAYER_NOT_FOUND.create();
    return player;
  }
}
