package land.chipmunk.chipmunkbot.commands;

import land.chipmunk.chipmunkbot.command.*;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.kyori.adventure.text.Component;

public class TestCommand {
  public static void register (CommandDispatcher dispatcher) {
    final TestCommand instance = new TestCommand();

    dispatcher.register(
      literal("test")
        .executes(instance::helloWorld)
    );
  }

  public int helloWorld (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();
    source.sendOutput(Component.text("Hello, world!"));

    return 1;
  }
}
