package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.song.*;
// import com.github.steveice10.packetlib.packet.Packet;
// import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.SessionListener;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.event.session.DisconnectedEvent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import lombok.Getter;
import lombok.Setter;
import java.io.File;
import java.nio.file.Path;
import java.net.URL;
import java.util.LinkedList;

public class SongPlayer extends SessionAdapter {
  private final ChipmunkBot client;

  public static final String SELECTOR  = "@a[tag=!nomusic,tag=!chipmunkbot_nomusic]";
  public static File SONG_DIR = new File("songs");
  static {
    if (!SONG_DIR.exists()) {
      SONG_DIR.mkdir();
    }
  }

  @Getter @Setter private Song currentSong;
  @Getter @Setter private LinkedList<Song> songQueue = new LinkedList<>();
  @Getter @Setter private SongLoaderThread loaderThread;
  private int ticksUntilPausedActionbar = 20;

  public SongPlayer (ChipmunkBot client) {
    this.client = client;
    client.addListener((SessionListener) this);
    client.core().addListener(new CommandCore.Listener() {
      public void ready () { coreReady(); } // TODO: Handle listeners in a better way than this
    });
    client.tickLoop().addListener(new TickLoop.Listener() {
      public void onTick (long t) { tick(); } // TODO: Handle listeners in a better way than this
    });
  }

  // TODO: Less duplicate code

  public void loadSong (Path location) {
    if (loaderThread != null) {
      client.chat().tellraw(Component.translatable("Already loading a song, cannot load another", NamedTextColor.RED));
      return;
    }

    try {
      final SongLoaderThread _loaderThread = new SongLoaderThread(location);
      client.chat().tellraw(Component.translatable("Loading %s", Component.text(location.getFileName().toString(), NamedTextColor.DARK_GREEN)).color(NamedTextColor.GREEN));
      _loaderThread.start();
      loaderThread = _loaderThread;
    } catch (SongLoaderException e) {
      client.chat().tellraw(Component.translatable("Failed to load song: %s", e.message()).color(NamedTextColor.RED));
      loaderThread = null;
    }
  }

  public void loadSong (URL location) {
    if (loaderThread != null) {
      client.chat().tellraw(Component.translatable("Already loading a song, cannot load another", NamedTextColor.RED));
      return;
    }

    try {
      final SongLoaderThread _loaderThread = new SongLoaderThread(location);
      client.chat().tellraw(Component.translatable("Loading %s", Component.text(location.toString(), NamedTextColor.DARK_GREEN)).color(NamedTextColor.GREEN));
      _loaderThread.start();
      loaderThread = _loaderThread;
    } catch (SongLoaderException e) {
      client.chat().tellraw(Component.translatable("Failed to load song: %s", e.message()).color(NamedTextColor.RED));
      loaderThread = null;
    }
  }

  public void coreReady () {
    if (currentSong != null) currentSong.play();
  }

  public void tick () {
    if (!client.core().ready()) return;

    if (loaderThread != null && !loaderThread.isAlive()) {
      if (loaderThread.exception != null) {
        client.chat().tellraw(Component.translatable("Failed to load song: %s", loaderThread.exception.message()).color(NamedTextColor.RED));
      } else {
        songQueue.add(loaderThread.song);
        client.chat().tellraw(Component.translatable("Added %s to the song queue", Component.empty().append(loaderThread.song.name).color(NamedTextColor.DARK_GREEN)).color(NamedTextColor.GREEN));
      }
      loaderThread = null;
    }

    if (currentSong == null) {
      if (songQueue.size() == 0) return;

      currentSong = songQueue.poll();
      client.chat().tellraw(Component.translatable("Now playing %s", Component.empty().append(currentSong.name).color(NamedTextColor.DARK_GREEN)).color(NamedTextColor.GREEN));
      currentSong.play();
    }

    if (currentSong.paused && ticksUntilPausedActionbar-- < 0) return;
    else ticksUntilPausedActionbar = 20;

    client.core().run("title " + SELECTOR + " actionbar " + GsonComponentSerializer.gson().serialize(generateActionbar()));

    if (currentSong.paused) return;

    handlePlaying();

    if (currentSong.finished()) {
      client.chat().tellraw(Component.translatable("Finished playing %s", Component.empty().append(currentSong.name).color(NamedTextColor.DARK_GREEN)).color(NamedTextColor.GREEN));
      currentSong = null;
    }
  }

  public Component generateActionbar () {
    Component component = Component.empty()
      .append(Component.translatable("Now playing %s", Component.empty().append(currentSong.name).color(NamedTextColor.DARK_GREEN)).color(NamedTextColor.GREEN))
      .append(Component.translatable(" | ", NamedTextColor.DARK_GRAY))
      .append(Component.translatable("%s / %s", formatTime(currentSong.time).color(NamedTextColor.GREEN), formatTime(currentSong.length).color(NamedTextColor.GREEN)).color(NamedTextColor.GRAY))
      .append(Component.translatable(" | ", NamedTextColor.DARK_GRAY))
      .append(Component.translatable("%s / %s", Component.text(currentSong.position, NamedTextColor.GREEN), Component.text(currentSong.size(), NamedTextColor.GREEN)).color(NamedTextColor.GRAY));

    if (currentSong.paused) {
      return component
        .append(Component.translatable(" | ", NamedTextColor.DARK_GRAY))
        .append(Component.translatable("Paused", NamedTextColor.DARK_GREEN));
    }

    if (currentSong.looping) {
      if (currentSong.loopCount > 0) {
        return component
          .append(Component.translatable(" | ", NamedTextColor.DARK_GRAY))
          .append(Component.translatable("Looping (%s/%s)", Component.text(currentSong.currentLoop), Component.text(currentSong.loopCount)).color(NamedTextColor.DARK_GREEN));
      }

      return component
        .append(Component.translatable(" | ", NamedTextColor.DARK_GRAY))
        .append(Component.translatable("Looping", NamedTextColor.DARK_GREEN));
    }

    return component;
  }

  public Component formatTime (long millis) {
    final int seconds = (int) millis / 1000;

    final String minutePart = String.valueOf(seconds / 60);
    final String unpaddedSecondPart = String.valueOf(seconds % 60);

    return Component.translatable(
      "%s:%s",
      Component.text(minutePart),
      Component.text(unpaddedSecondPart.length() < 2 ? "0" + unpaddedSecondPart : unpaddedSecondPart)
    );
  }

  public void stopPlaying () {
    currentSong = null;
  }

  @Override
  public void disconnected (DisconnectedEvent event) {
    if (currentSong != null) currentSong.pause();
  }

  public void handlePlaying () {
    currentSong.advanceTime();
    while (currentSong.reachedNextNote()) {
      final Note note = currentSong.getNextNote();

      final double floatingPitch = Math.pow(2, (note.pitch - 12) / 12.0);

      client.core().run("execute as " + SELECTOR + " at @s run playsound " + note.instrument.sound + " record @s ~ ~ ~ " + note.volume + " " + floatingPitch);
    }
  }
}
