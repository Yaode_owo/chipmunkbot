package land.chipmunk.chipmunkbot.commands;

import land.chipmunk.chipmunkbot.util.Logging;
import land.chipmunk.chipmunkbot.command.*;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import lombok.AllArgsConstructor;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.zip.GZIPInputStream;

public class LogQueryCommand {
  private LogQueryThread thread;

  private static SimpleCommandExceptionType ALREADY_QUERYING_LOGS_EXCEPTION = new SimpleCommandExceptionType(ComponentMessage.wrap(Component.translatable("Another log query is already running")));
  private static SimpleCommandExceptionType NOT_QUERYING_LOGS_EXCEPTION = new SimpleCommandExceptionType(ComponentMessage.wrap(Component.translatable("No log query is currently running")));

  public static void register (CommandDispatcher dispatcher) {
    final LogQueryCommand instance = new LogQueryCommand();

    dispatcher.register(
      literal("logquery")
        .then(
          literal("abort")
            .executes(instance::abortCommand)
        )
        .then(
          literal("count")
            .then(
              argument("text", greedyString())
                .executes(instance::countCommand)
            )
        )
        .then(
          literal("report")
            .then(
              argument("text", greedyString())
                // .executes(instance::reportCommand)
            )
        )
        .then(
          literal("progress")
            .executes(instance::progressCommand)
        )
    );
  }

  public boolean querying () {
    if (thread == null) return false;
    if (thread.isAlive()) return true;
    thread = null;
    return false;
  }

  public void alreadyQueryingCheck () throws CommandSyntaxException {
    if (!querying()) return;
    throw ALREADY_QUERYING_LOGS_EXCEPTION.create();
  }

  public int abortCommand (CommandContext<CommandSource> context) throws CommandSyntaxException {
    if (!querying()) throw NOT_QUERYING_LOGS_EXCEPTION.create();

    thread.breakQuery();
    context.getSource().sendOutput(Component.translatable("Stopped querying logs"));

    return Command.SINGLE_SUCCESS;    
  }

  public int countCommand (CommandContext<CommandSource> context) throws CommandSyntaxException {
    alreadyQueryingCheck();

    final CommandSource source = context.getSource();

    final String text = getString(context, "text");

    thread = new CountThread(line -> line.contains(text), Logging.LOGS_DIR, source);
    thread.start();

    source.sendOutput(Component.translatable("Searching for instances of %s in the logs", Component.text(text, NamedTextColor.GREEN)));

    return Command.SINGLE_SUCCESS;
  }

  public int progressCommand (CommandContext<CommandSource> context) {
    final Component progress = Component.translatable(
      "Currently reading %s (%s/%s)",
      Component.text(thread.currentFilename, NamedTextColor.GREEN),
      Component.text(thread.currentIndex + 1),
      Component.text(thread.totalFiles)
    );

    context.getSource().sendOutput(progress, false);
    return Command.SINGLE_SUCCESS;
  }

  public static abstract class LogQueryThread extends Thread {
    public Predicate<String> query;
    public File directory;

    public String currentFilename = null;
    public int currentIndex = 0;
    public int totalFiles;

    private boolean breakFlag;

    public void run () {
      breakFlag = false;

      String[] filenames = directory.list();
      if (filenames == null) return;
      Arrays.sort(filenames);
      totalFiles = filenames.length;

      onStart();

      for (String filename : filenames) {
        currentFilename = filename;

        if (breakFlag) return;

        try {
          InputStream is = new FileInputStream(new File(directory, filename));
          if (filename.endsWith(".log.gz")) is = new GZIPInputStream(is);
          else if (!filename.endsWith(".log")) continue;

          final Scanner scanner = new Scanner(is, StandardCharsets.UTF_8);

          while (scanner.hasNextLine()) {
            final String line = scanner.nextLine();
            if (!query.test(line)) continue;
            onMatch(line);
          }

          scanner.close();
        } catch (Exception exception) {
          exception.printStackTrace();
        }

        currentIndex++;
      }
    }

    public void breakQuery () {
      breakFlag = true;
    }

    public void onStart () {}
    public void onFinish () {}
    abstract void onMatch (String line);
  }

  @AllArgsConstructor
  public static class CountThread extends LogQueryThread {
    public int instances = 0;
    public long startTime;
    public CommandSource source;

    public CountThread (Predicate<String> query, File directory, CommandSource source) {
      this.query = query;
      this.directory = directory;
      this.source = source;
    }

    public void onStart () {
      startTime = System.currentTimeMillis();
    }

    public void onMatch (String line) {
      instances++;
    }

    public void onFinish () {
      double seconds = (double) (System.currentTimeMillis() - startTime) / 1000d;
      source.sendOutput(Component.translatable("Found %s instances of the specified query in %s seconds", Component.text(instances, NamedTextColor.GREEN), Component.text(seconds, NamedTextColor.GREEN)));
    }
  }
}
